FROM tomcat:8.0-jre8-alpine

ENV ORDS_HOME=/opt/oracle/ords \
    ORDS_VERSION=17.4.1.353.06.48
    DB_SERVICENAME=prod \
    DB_USERNAME=www_user \
    DB_PASSWORD=password \
    DB_HOSTNAME=oracle.example.edu \
    DB_PORT=1521 \
    DB_INITiALSIZE=25 \
    DB_MAXTOTAL=100


RUN mkdir -p $ORDS_HOME && mkdir -p $ORDS_HOME/params

COPY ords/ords.war $ORDS_HOME/ords.war

COPY run.sh $ORDS_HOME

RUN chmod +x $ORDS_HOME/run.sh && rm -rf /usr/local/tomcat/webapps/*

CMD $ORDS_HOME/run.sh