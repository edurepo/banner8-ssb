#!/bin/bash

cat > $ORDS_HOME/params/ords_params.properties <<EOF
db.hostname=$DB_HOSTNAME
db.password=$DB_PASSWORD
db.port=$DB_PORT
db.servicename=$DB_SERVICENAME
db.username=$DB_USERNAME
plsql.gateway.add=true
rest.services.apex.add=false
rest.services.ords.add=false
standalone.mode=false
EOF

if [ ! -f $ORDS_HOME/config/ords/standalone/standalone.properties ]; then
cd $ORDS_HOME
java -jar ords.war configdir $ORDS_HOME/config
java -jar ords.war install simple

cp -arv $ORDS_HOME/config/ords $ORDS_HOME/config/$DB_SERVICENAME
fi;


cp $ORDS_HOME/ords.war /usr/local/tomcat/webapps/$DB_SERVICENAME.war

java -jar ords.war set-property jdbc.MaxLimit $DB_MAXTOTAL
java -jar ords.war set-property jdbc.MinLimit $DB_INITIALSIZE
java -jar ords.war set-property jdbc.InitialLimit $DB_INITIALSIZE

if [ ! -v $ORDS_DEBUG ]; then
  java -jar ords.war set-property debug.debugger true
  java -jar ords.war set-property debug.printDebugToScreen true
fi

cp $ORDS_HOME/config/ords/defaults.xml $ORDS_HOME/config/$DB_SERVICENAME/defaults.xml

exec catalina.sh run