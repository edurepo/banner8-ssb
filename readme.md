# Bandock Banner 8 SSB ORDS

This image allows Banner SSB 8 to run on ORDS (Oracle REST Data Services) on tomcat.

## How to use

### Build

In order to build this container you will need to download ords from oracle.  It is available at http://www.oracle.com/technetwork/developer-tools/rest-data-services/downloads/index.html .  Once downloaded unzip the ords.$version.zip to the directory ords. Then you can build the container. You should update the ORDS_VERSION in the Dockerfile to be the version you downloaded.

```Shell
mkdir ords
unzip ords.17.4.1.353.06.48.zip -d ords

docker build -t banner8-ssb:ords .

```
### Run

```Shell
docker run -e "DB_SERVICENAME=prod" -e "DB_USERNAME=www_user" -e "DB_PASSWORD=password" -e "DB_HOSTNAME=oracle.example.edu" banner8-ssb:ords 
```

### Environment Variables

```Shell
ORDS_HOME - default: /opt/oracle/ords
ORDS_VERSION
DB_SERVICENAME - default: prod. Database instance to connect to
DB_USERNAME - default: www_user.  Database user
DB_PASSWORD - default: password.  Database user's password
DB_HOSTNAME - default: oracle.example.edu.  Database hostname
DB_PORT - default: 1521. Database port
DB_INITiALSIZE - default: 25. inital connection pool size
DB_MAXTOTAL - default: 100.  Max db conenctions
DEBUG - optional.  Set to true if you want to enable ords debug options.
```
